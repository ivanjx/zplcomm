﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Printing;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace zplcomm;

public class RelayCommand : ICommand
{
    Action m_action;

    public event EventHandler? CanExecuteChanged;

    public RelayCommand(Action action)
    {
        m_action = action;
    }

    public bool CanExecute(object? parameter)
    {
        return true;
    }

    public void Execute(object? parameter)
    {
        m_action.Invoke();
    }
}

public partial class MainWindow : Window, INotifyPropertyChanged
{
    PrintService m_printService;

    string[] m_printers;
    string? m_selectedPrinter;
    string m_zplStr;

    public event PropertyChangedEventHandler? PropertyChanged;

    public string[] Printers
    {
        get => m_printers;
        set
        {
            m_printers = value;
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(Printers)));
        }
    }

    public string? SelectedPrinter
    {
        get => m_selectedPrinter;
        set
        {
            m_selectedPrinter = value;
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(SelectedPrinter)));
        }
    }

    public string ZplStr
    {
        get => m_zplStr;
        set
        {
            m_zplStr = value;
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(ZplStr)));
        }
    }

    public ICommand PrintCommand
    {
        get => new RelayCommand(Print);
    }

    public MainWindow()
    {
        InitializeComponent();
        DataContext = this;

        m_printService = new PrintService();
        m_printers = Array.Empty<string>();
        m_zplStr = "^XA^LH30,30\r\n^FO20,10^ADN,90,50^AD^FDHello World^FS\r\n^XZ";

        Loaded += HandleLoad;
    }

    void HandleLoad(object sender, RoutedEventArgs e)
    {
        LoadPrinters();
    }

    void LoadPrinters()
    {
        try
        {
            List<string> printers = new List<string>();

            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                printers.Add(printer);
            }

            Printers = printers.ToArray();
        }
        catch (Exception ex)
        {
            MessageBox.Show(
                "Unable to load printers: " + ex.Message,
                "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
        }
    }

    void Print()
    {
        if (SelectedPrinter == null)
        {
            MessageBox.Show(
                "Printer not selected",
                "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
            return;
        }

        string zplStr = Regex.Replace(ZplStr, @"\s", "");

        if (string.IsNullOrEmpty(zplStr))
        {
            MessageBox.Show(
                "No zpl to be sent",
                "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
            return;
        }

        try
        {
            m_printService.PrintZpl(
                SelectedPrinter,
                zplStr);
            MessageBox.Show(
                "ZPL command sent",
                "Information",
                MessageBoxButton.OK,
                MessageBoxImage.Information);
        }
        catch (Exception ex)
        {
            MessageBox.Show(
                "Unable to send to printer: " + ex.Message,
                "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
        }
    }
}