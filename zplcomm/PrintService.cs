﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace zplcomm;

public class PrintService
{
    public void PrintZpl(string printerName, string zpl)
    {
        bool success = Native.OpenPrinterA(
            printerName,
            out nint hPrinter,
            nint.Zero);

        if (!success)
        {
            throw new Exception("OpenPrinterA: " + Marshal.GetLastWin32Error());
        }

        Native.DOC_INFO_1 docInfo = new Native.DOC_INFO_1()
        {
            pDocName = "Document",
            pDataType = "RAW",
            pOutputFile = null
        };
        success = Native.StartDocPrinterA(
            hPrinter,
            1,
            ref docInfo);

        if (!success)
        {
            Native.ClosePrinter(hPrinter);
            throw new Exception("StartDocPrinterA: " + Marshal.GetLastWin32Error());
        }

        success = Native.StartPagePrinter(hPrinter);

        if (!success)
        {
            Native.EndDocPrinter(hPrinter);
            Native.ClosePrinter(hPrinter);
            throw new Exception("StartPagePrinter: " + Marshal.GetLastWin32Error());
        }

        if (!zpl.EndsWith("\n"))
        {
            zpl = zpl + "\r\n";
        }

        byte[] zplBuff = Encoding.ASCII.GetBytes(zpl);
        success = Native.WritePrinter(
            hPrinter,
            zplBuff,
            zplBuff.Length,
            out int written);

        Native.EndPagePrinter(hPrinter);
        Native.EndDocPrinter(hPrinter);
        Native.ClosePrinter(hPrinter);

        if (!success)
        {
            throw new Exception("WritePrinter: " + Marshal.GetLastWin32Error());
        }

        if (written != zplBuff.Length)
        {
            throw new Exception("Unable to send all print data");
        }
    }

    static class Native
    {
        const string WINSPOOL_DRV = "winspool.Drv";

        public struct DOC_INFO_1
        {
            public string pDocName;
            public string? pOutputFile;
            public string pDataType;
        }

        [DllImport(WINSPOOL_DRV, SetLastError = true)]
        public static extern bool OpenPrinterA(
            string szPrinter,
            out nint hPrinter,
            nint pd);

        [DllImport(WINSPOOL_DRV, SetLastError = true)]
        public static extern bool ClosePrinter(
            nint hPrinter);

        [DllImport(WINSPOOL_DRV, SetLastError = true)]
        public static extern bool StartDocPrinterA(
            nint hPrinter,
            int level,
            ref DOC_INFO_1 di);

        [DllImport(WINSPOOL_DRV, SetLastError = true)]
        public static extern bool EndDocPrinter(
            nint hPrinter);

        [DllImport(WINSPOOL_DRV, SetLastError = true)]
        public static extern bool StartPagePrinter(
            nint hPrinter);

        [DllImport(WINSPOOL_DRV, SetLastError = true)]
        public static extern bool EndPagePrinter(
            nint hPrinter);

        [DllImport(WINSPOOL_DRV, SetLastError = true)]
        public static extern bool WritePrinter(
            nint hPrinter,
            byte[] pBytes,
            int dwCount,
            out int dwWritten);
    }
}
